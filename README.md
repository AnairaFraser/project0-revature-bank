# PROJECT 0

## Description

A console-based application simulating banking operations.

## Technology Used

* Java 8
  * OOP
  * JUnit Testing
  * JDBC/DAO
* SQL (Oracle)
* AWS
* DBeaver


## Features

#### Available Features
* Users 
  * Creating accounts (customer, employee, or admin)
  * Logging in/logging out of accounts
  * All users (provided that their account is active) are able to view their balance, make deposits, withdraw money, and transfer money to other users
  * Employees and admins (collectively known as staff) have the additional abilities to view user accounts
* System
  * Rejects invalid user inputs (i.e. negative transactions)
  * Transcribing the session's transactions to a text file
  * JUnit file to test method functionality
  * SQL database & JDBC code for data access

#### To-Do List

* Implement joint accounts
* Streamline code structure for easier readability

## Getting Started/Usage

* Downloading Project:
```
git clone https://gitlab.com/AnairaFraser/project0-revature-bank
```
* Must have [DBeaver](https://dbeaver.io) installed in order to interact with project
* Create a new Oracle connection in DBeaver using the following credentials: (do not forget to disconnect when finished using project)
  * Username: admin
  * Password: foobarre
  * URL: jdbc:oracle:thin:@database-1.cdaukxcx8a8t.us-east-2.rds.amazonaws.com:1521:ORCL
* Primarily a menu-based application; select the option you want by entering its corresponding number. Otherwise, just follow the prompts as directed in the console.
* For example:

![Sample](https://gitlab.com/AnairaFraser/project0-revature-bank/-/blob/main/Sample.png)

* Note: do not run the BankDAOTester file without truncating the database tables first
